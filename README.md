# astra-liveloader

Contains 2 projects - a hello world ASTRA program and a sample class that loads the ASTRA program from a jar file at runtime.

# Usage

* Package the `astra-hello` project using `mvn package`
* Copy the `astra-hello-1.0.0.jar` file into the `astra-loader/agents` folder.
* Run the `astra-loader` project using `mvn compile exec:java`

